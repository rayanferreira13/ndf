## Gestion des utilisateurs

1)	Un utilisateur peut se connecter

## Déclaration ndf // role user

1)	Un utilisateur crée une liste de frais
2)	Dans une liste, il peut ajouter différents types de frais
3)	Un utilisateur peut passer le statut de la liste en « à valider »
4)	Une liste en « à valider » ne peut plus être modifiée

## Validation des ndf // role validateur
1)	Un validateur voit la liste des ndf en statut « à valider »
2)	Un validateur peut passer le statut d’une note en « refusée » ou en « validée »


## Règles de calcul des indemnités km


**Montant :**  montant en euro (calculé si type kilométrique)

Types :
*	Frais kilométrique : http://impotsurlerevenu.org/nouveautes-impot-2020/1253-bareme-kilometrique-2020.php
*	Frais génériques : Doit avoir une facturette associée



